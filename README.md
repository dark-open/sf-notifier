# SF-Notifier
**WIP**  
This project is an example of using the Symony/Notifier component to illustrate an article on my blog.

If you are comfortable with French, you will find more explanations in the article (In progress).

https://darkblog.lhoir.me
## ⚠️ Disclaimer

This project **should not** be used in production without adding a good number of controls and optimizations.

## 🙇 Author
#### Simon Lhoir aka DarkChyper
- Mastodon 🐘 : [@Dark_Chyper](https://pouet.chapril.org/@Dark_Chyper)
- Github: [@DarkChyper](https://github.com/DarkChyper)
- Gitlab: [@DarkChyper](https://gitlab.com/DarkChyper)


## ➤ License
Distributed under the Creative Commons Zero v1.0 Universal License. See [LICENSE](LICENSE) for more information.