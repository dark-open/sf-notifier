<?php

namespace App\Controller;

use App\Entity\ChatNotifier;
use App\Form\ChatNotifierAddType;
use App\Form\ChatNotifierType;
use App\Form\MessageType;
use App\Repository\ChatNotifierRepository;
use App\Transport\TransportChatCollection;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Notifier\Message\ChatMessage;
use Symfony\Component\Routing\Attribute\Route;

class NotifierController extends AbstractController
{
    #[Route('/', name: 'app.notifier.index')]
    public function index(
        Request                $request,
        ChatNotifierRepository $chatNotifierRepository
    ): Response
    {
        #Ideally, the controller should be minimized and everything should happen
        #(fetching and saving to the database) in services

        $chatNotifierArray = $chatNotifierRepository->findAll();

        $addForm = $this->createForm(ChatNotifierAddType::class);
        $addForm->handleRequest($request);

        if ($addForm->isSubmitted() && $addForm->isValid()) {
            $chatNotifier = $addForm->getData();

            return $this->redirectToRoute('app.notifier.new', ['transportClassName' => $chatNotifier['transport']]);
        }

        return $this->render('notifier/index.html.twig', [
            'chatNotifierArray' => $chatNotifierArray,
            'addForm' => $addForm
        ]);
    }

    #[Route("/new/{transportClassName}", name: "app.notifier.new")]
    #[Route("/edit/{chatNotifier}", name: "app.notifier.edit")]
    public function edit(
        Request                 $request,
        EntityManagerInterface  $em,
        TransportChatCollection $transportChatCollection,
        ChatNotifier            $chatNotifier = null,
        string                  $transportClassName = null
    ): Response
    {
        $transportChat = null;
        if (isset($transportClassName)) {
            $transportChat = $transportChatCollection->getTransportChatByClassName($transportClassName);

            $chatNotifier = new ChatNotifier();
            $chatNotifier->setTransport($transportChat->getName());
        }

        if (is_null($transportChat)) {
            $transportChat = $transportChatCollection->getTransportChatByChatNotifierEntity($chatNotifier);
        }

        $form = $this->createForm(ChatNotifierType::class, $chatNotifier);
        $form->handleRequest($request);

        if ($form->isSubmitted() and $form->isValid()) {
            $em->persist($chatNotifier);

            $message = 'Notifier added with success';
            if ($chatNotifier->getId() !== null) {
                $message = 'Notifier modified with success';
            }
            $em->flush();

            $this->addFlash('success', $message);
            return $this->redirectToRoute('app.notifier.index');
        }

        return $this->render('notifier/edit.html.twig', [
            'chatNotifier' => $chatNotifier,
            'form' => $form
        ]);

    }

    #[Route('/{chatNotifier}/send', name: 'app.notifier.send')]
    public function send(Request $request, ChatNotifier $chatNotifier, TransportChatCollection $transportChatCollection): Response
    {
        $form = $this->createForm(MessageType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $transport = $transportChatCollection->getTransportChatByClassName($chatNotifier->getTransport());
            $transport->defineTransport($chatNotifier);
            $transport->send(new ChatMessage($form->getData()['message']));
            $this->addFlash('success', 'The message has been sent');

            return $this->redirectToRoute('app.notifier.index');
        }

        return $this->render('notifier/send.html.twig', [
            'form' => $form,
            'chatNotifier' => $chatNotifier,
        ]);
    }

    #[Route('/{chatNotifier}/remove', name: 'app.notifier.remove')]
    public function delete(EntityManagerInterface $em, ChatNotifier $chatNotifier): Response
    {
        $message = 'Chat notifier "' . $chatNotifier->getLabel() . '" has been removed successfully';
        $em->remove($chatNotifier);
        $em->flush();

        $this->addFlash('success', $message);

        return $this->redirectToRoute('app.notifier.index');
    }
}
