<?php

namespace App\Entity;

use App\Repository\ChatNotifierRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Entity(repositoryClass: ChatNotifierRepository::class)]
class ChatNotifier
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    #[Assert\NotBlank()]
    private string $label = '';

    #[ORM\Column(length: 255)]
    #[Assert\NotBlank()]
    private string $transport = '';

    #[ORM\Column(type: Types::TEXT)]
    #[Assert\NotBlank()]
    private string $token = '';

    #[ORM\Column]
    private array $params = [];

    public function __set(string $name, mixed $value): void
    {
        $this->setParam($name, $value);
    }

    public function __get(string $name): mixed
    {
        return $this->getParam($name);
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLabel(): string
    {
        return $this->label;
    }

    public function setLabel(string $label): static
    {
        $this->label = $label;

        return $this;
    }

    public function getTransport(): string
    {
        return $this->transport;
    }

    public function setTransport(string $transport): static
    {
        $this->transport = $transport;

        return $this;
    }

    public function getToken(): string
    {
        return $this->token;
    }

    public function setToken(string $token): static
    {
        $this->token = $token;

        return $this;
    }

    public function getParams(): array
    {
        return $this->params;
    }

    public function setParams(array $params): static
    {
        $this->params = $params;

        return $this;
    }

    public function setParam(string $name, mixed $value): ChatNotifier
    {
        $this->params[$name] = $value;

        return $this;
    }

    public function getParam(string $name): mixed
    {
        if (isset($this->params[$name])) {
            return $this->params[$name];
        }

        return null;
    }
}
