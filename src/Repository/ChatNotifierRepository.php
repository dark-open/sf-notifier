<?php

namespace App\Repository;

use App\Entity\ChatNotifier;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<ChatNotifier>
 *
 * @method ChatNotifier|null find($id, $lockMode = null, $lockVersion = null)
 * @method ChatNotifier|null findOneBy(array $criteria, array $orderBy = null)
 * @method ChatNotifier[]    findAll()
 * @method ChatNotifier[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ChatNotifierRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ChatNotifier::class);
    }

}
