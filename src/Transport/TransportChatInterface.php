<?php

namespace App\Transport;

use App\Entity\ChatNotifier;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Notifier\Message\ChatMessage;
use Symfony\Component\Notifier\Message\SentMessage;

interface TransportChatInterface
{
    public function getLabel(): string;

    public function getName(): string;

    public function getDescription(): string;

    public function getFields(FormBuilderInterface $form): FormBuilderInterface;

    public function defineTransport(ChatNotifier $chatNotifier): TransportChatInterface;

    # To go further in the implementation, it would be necessary to create a standard message per transport.
    # Each transport message would share the same interface which would be used below
    # The "send" method would then convert the typed message into "Symfony\Component\Notifier\Message\ChatMessage"
    # with the transport options like Poll in Mastodon or Color in Teams
    public function send(ChatMessage $chatMessage): SentMessage;
}