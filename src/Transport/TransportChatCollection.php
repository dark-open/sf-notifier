<?php

namespace App\Transport;

use App\Entity\ChatNotifier;
use JetBrains\PhpStorm\NoReturn;

class TransportChatCollection
{
    /** @var array<string,TransportChatInterface> */
    private static array $transportChats = [];

    #[NoReturn] public function __construct(iterable $chatTransports)
    {
        if (empty(self::$transportChats)) {
            /** @var TransportChatInterface $chatTransport */
            foreach ($chatTransports as $chatTransport) {
                $reflexion = new \ReflectionClass($chatTransport);
                self::$transportChats[$reflexion->getShortName()] = $chatTransport;
            }
        }
    }

    public function getTransportChats(): array
    {
        return self::$transportChats;
    }

    /**
     * @throws \Exception
     */
    public function getTransportChatByClassName(string $transportCClassName): TransportChatInterface
    {
        if (isset(self::$transportChats[$transportCClassName])) {
            return clone self::$transportChats[$transportCClassName];
        }

        throw new \Exception('Unable to find service tagging "notifier.transport.chat" named '
            . $transportCClassName);
    }

    public function getTransportChatByChatNotifierEntity(ChatNotifier $chatNotifier): TransportChatInterface
    {
        if (isset(self::$transportChats[$chatNotifier->getTransport()])) {
            return clone self::$transportChats[$chatNotifier->getTransport()];
        }

        throw new \Exception('Unable to find service tagging "notifier.transport.chat" named '
            . $chatNotifier->getTransport());
    }
}