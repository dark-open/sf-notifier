<?php

namespace App\Transport\Chat;

use App\Entity\ChatNotifier;
use App\Transport\AbstractTransportChat;
use App\Transport\TransportChatInterface;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Notifier\Bridge\Slack\SlackTransportFactory;
use Symfony\Component\Notifier\Exception\TransportExceptionInterface;
use Symfony\Component\Notifier\Message\ChatMessage;
use Symfony\Component\Notifier\Message\SentMessage;
use Symfony\Component\Notifier\Transport\Dsn;
use Symfony\Component\Notifier\Transport\TransportInterface;

class TransportChatSlack extends AbstractTransportChat
{
    private const DSN_PREFIX = 'slack://';
    private const TOKEN = '/^xoxb-.*/';

    protected ?TransportInterface $transport = null;

    public function getLabel(): string
    {
        return 'Slack';
    }

    public function getName(): string
    {
        return 'TransportChatSlack';
    }

    public function getDescription(): string
    {
        return 'Send message to a slack channel';
    }

    public function getFields(FormBuilderInterface $form): FormBuilderInterface
    {
        $form
            ->add('channel', TextType::class, [
                'required' => true,
                'label' => 'Slack channel ID'
            ]);

        // it could be a good idea to add PRE-SUBMIT EventListener here
        // to avoid saving blank or bad channel format in database
        return $form;
    }

    /**
     * @throws \Exception
     */
    public function defineTransport(ChatNotifier $chatNotifier): TransportChatInterface
    {
        if (is_null($this->transport)) {
            if (!preg_match(self::TOKEN, $chatNotifier->getToken())) {
                throw new \Exception('Token must starts with "xoxb-"');
            }

            if (!in_array('channel', array_keys($chatNotifier->getParams()))) {
                throw new \Exception('No channel defined in the slack parameter');
            }

            $scheme =
                self::DSN_PREFIX
                . $chatNotifier->getToken()
                . '@default?channel='
                . $chatNotifier->getParam('channel');

            $dsn = new Dsn($scheme);

            $factory = new SlackTransportFactory();
            $this->transport = $factory->create($dsn);
        }

        return $this;
    }

    /**
     * @throws TransportExceptionInterface
     * @see TransportChatInterface
     */
    public function send(ChatMessage $chatMessage): SentMessage
    {
        return $this->transport->send($chatMessage);
    }
}