<?php

namespace App\Transport\Chat;

use App\Entity\ChatNotifier;
use App\Transport\AbstractTransportChat;
use App\Transport\TransportChatInterface;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Notifier\Bridge\Mastodon\MastodonTransportFactory;
use Symfony\Component\Notifier\Exception\TransportExceptionInterface;
use Symfony\Component\Notifier\Message\ChatMessage;
use Symfony\Component\Notifier\Message\SentMessage;
use Symfony\Component\Notifier\Transport\Dsn;
use Symfony\Component\Notifier\Transport\TransportInterface;

class TransportChatMastodon extends AbstractTransportChat
{
    private const DSN_PREFIX = 'mastodon://';

    protected ?TransportInterface $transport = null;

    public function getLabel(): string
    {
        return 'Mastodon';
    }

    public function getName(): string
    {
        return 'TransportChatMastodon';
    }

    public function getDescription(): string
    {
        return 'Send message to a Mastodon instance';
    }

    public function getFields(FormBuilderInterface $form): FormBuilderInterface
    {
        $form
            ->add('host', TextType::class, [
                'required' => true,
                'label' => 'Mastodon URL HOST'
            ]);

        // it could be a good idea to add PRE-SUBMIT EventListener here
        // to avoid saving empty host in database
        return $form;
    }

    /**
     * @throws \Exception
     */
    public function defineTransport(ChatNotifier $chatNotifier): TransportChatInterface
    {
        if (is_null($this->transport)) {
            dump($chatNotifier->getParams());
            if (!in_array('host', array_keys($chatNotifier->getParams()))) {
                throw new \Exception('No host defined in the Mastodon parameter');
            }

            $scheme =
                self::DSN_PREFIX
                . $chatNotifier->getToken()
                . '@'
                . $chatNotifier->getParam('host');

            $dsn = new Dsn($scheme);

            $factory = new MastodonTransportFactory();
            $this->transport = $factory->create($dsn);
        }

        return $this;
    }

    /**
     * @throws TransportExceptionInterface
     * @see TransportChatInterface
     */
    public function send(ChatMessage $chatMessage): SentMessage
    {
        return $this->transport->send($chatMessage);
    }
}