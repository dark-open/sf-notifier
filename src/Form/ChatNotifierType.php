<?php

namespace App\Form;

use App\Entity\ChatNotifier;
use App\Transport\TransportChatCollection;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;

class ChatNotifierType extends AbstractType
{
    protected TransportChatCollection $transportChatCollection;

    public function __construct(TransportChatCollection $transportChatCollection)
    {
        $this->transportChatCollection = $transportChatCollection;
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        /** @var ChatNotifier $chatNotifier */
        $chatNotifier = $builder->getData();
        $transportChat = $this->transportChatCollection->getTransportChatByChatNotifierEntity($chatNotifier);

        $builder
            ->add('label', TextType::class, [
                'required' => true,
            ])
            ->add('token', TextType::class, [
                'label' => 'Token',
                'required' => true,
                'constraints' => array(
                    new NotBlank(),
                )
            ]);

        $builder = $transportChat->getFields($builder);

        $builder->add('save', SubmitType::class);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => ChatNotifier::class,
        ]);
    }
}
