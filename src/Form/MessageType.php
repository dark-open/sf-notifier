<?php

namespace App\Form;

use App\Transport\TransportChatCollection;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

class MessageType extends AbstractType
{
    protected TransportChatCollection $transportChatCollection;

    public function __construct(TransportChatCollection $transportChatCollection)
    {
        $this->transportChatCollection = $transportChatCollection;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('message', TextType::class, array(
                'required' => true,
            ))
            ->add('Send', SubmitType::class);
    }
}