<?php

namespace App\Form;

use App\Transport\TransportChatCollection;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;

class ChatNotifierAddType extends AbstractType
{
    protected TransportChatCollection $transportChatCollection;

    public function __construct(TransportChatCollection $transportChatCollection)
    {
        $this->transportChatCollection = $transportChatCollection;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $transportChats = $this->transportChatCollection->getTransportChats();
        $transportChoices = array_map(
            function ($transportChat) {
                return $transportChat->getLabel();
            },
            $transportChats
        );
        $transportChoices = array_flip($transportChoices);
        ksort($transportChoices);

        $builder
            ->add('transport', ChoiceType::class, array(
                'choices' => $transportChoices
            ))
            ->add('add', SubmitType::class);
    }
}